# Loan Application

This application mostly comes from my assumption since requirement is not detail.

## What Covered
Flow from new user register until their loan request is disbursed.

Trying to use simple microservice design (although it's too much for small scale application).

Do not apply Redis, Applicatin Monitoring, Error Logging. 

For this case, any validation are keep as function. Do not detailing every function. 

Class and Sequencce Diagram do not defined very detailed here. More try to explain how the system works and data flow.

### Class Diagram
Trying to describe what system will looks like. Trying to use MVC concept where have some Layers:

1. Repository, responsible to communicate with Database (CRUD)
2. Service, responsible as main logic, do validation and get needed data through Repository.
3. Controller, responsible for receiver request and give to suitable Service.

Dot not connecting Layers with model to decrease tangled arrows.

### Sequence Diagram
In sequence diagram, I put high level view (sequence number 00). 

Also every funtion called from client will through authorization sequence (number 0) before go through targeted service.

### Appliation Mockup
For mockup appliation also can be found here:
https://www.figma.com/file/Zzub70JJxIZlysz9J3P7n4/Mandiri?type=design&node-id=0%3A1&mode=design&t=YhPHJCufCsQAKZOo-1


